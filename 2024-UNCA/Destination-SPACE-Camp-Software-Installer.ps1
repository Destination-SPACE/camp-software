#####################################################################################
#   Destination SPACE Satellite Week 2024 - UNCA Camp Software Installer
#
#   Description:
#   This PowerShell script installs required programs for student computers
#
#   Installed Software:
#   - Microsoft Visual Studio Code
#       - C/C++ extension
#       - Serial Monitor extension
#       - PlatformIO IDE Extension
#   - OpenRocket
#   - Git
#
#   modified 2024-04-22
#   by Madison Gleydura
#
#   THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
#   FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#   COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
#   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#####################################################################################

$scriptPath = split-path -parent $MyInvocation.MyCommand.Definition

Write-Host "Destination SPACE Software Installer"

$exeArgs = '/verysilent /tasks=addcontextmenufiles,addcontextmenufolders,addtopath'

#Microsoft Visual Studio Code Installation
$vscodeFileUri = "https://code.visualstudio.com/sha/download?build=stable&os=win32-x64"
$vsCodeDestination = $scriptPath + "\VSCodeSetup-x64.exe"
$vsCodeExec = ($Env:PROGRAMFILES) + "\Microsoft VS Code\bin\code.cmd"
$extensions =@(
    "ms-vscode.cpptools",               #C/C++ Language Support
    "ms-vscode.vscode-serial-monitor",  #Serial Monitor Extension
    "platformio.platformio-ide"         #PlatformIO Support
) | SORT-OBJECT

Write-Host "Downloading Microsoft Visual Studio Code..."
$vsCodeBitsJobObj = Start-BitsTransfer $vscodeFileUri -Destination $vsCodeDestination

switch($vsCodeBitsJobObj.JobState){
    'Transferred'{
        Complete-BitsTransfer -BitsJob $vsCodeBitsJobObj
        break
    }
    'Error'{
        throw 'Error downloading Microsoft Visual Studio Code'
    }
}

Write-Host "Installing Microsoft Visual Studio Code..."
Start-Process -Wait $vsCodeDestination -ArgumentList $exeArgs

$extensions | ForEach-Object{
    try{
        Invoke-Expression "& '$vsCodeExec' --install-extension $_ --force"
        Write-Host
    }
    catch{
        $_
        Exit(1)
    }
}

#Open Rocket Installation
$openRocketFileUri = "https://github.com/openrocket/openrocket/releases/download/release-23.09/OpenRocket-23.09-Windows.exe"
$openRocketDestination = "OpenRocket-23.09-Windows.exe"

Write-Host "Downloading OpenRocket..."
$openRocketBitsJobObj = Start-BitsTransfer $openRocketFileUri -Destination $openRocketDestination

switch($openRocketBitsJobObj.JobObject){
    'Transfered'{
        Complete-BitsTransfer -BitsJob $openRocketBitsJobObj
        break
    }
    'Error'{
        throw 'Error downloading OpenRocket'
    }
}

Write-Host "Installing OpenRocket..."
Start-Process -Wait $openRocketDestination -ArgumentList $exeArgs

#Git Installation
$gitFileUri = "https://github.com/git-for-windows/git/releases/download/v2.44.0.windows.1/Git-2.44.0-64-bit.exe"
$gitDestination = "Git-2.44.0-64-bit.exe"

Write-Host "Downloading Git..."
$gitBitsObj = Start-BitsTransfer $gitFileUri -Destination $gitDestination

switch($gitBitsObj.JobObject){
    'Transfered'{
        Complete-BitsTransfer -BitsJob $gitBitsObj
        break
    }
    'Error'{
        throw 'Error downloading Git'
    }
}

Write-Host "Installing Git..."
Start-Process -Wait $gitDestination -ArgumentList $exeArgs

Exit(0)
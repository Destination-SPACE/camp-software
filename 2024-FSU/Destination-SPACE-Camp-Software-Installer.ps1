#####################################################################################
#   Destination SPACE Satellite Week 2024 - Fairmont State Camp Software Installer
#
#   Description:
#   This PowerShell script installs required programs for student computers
#
#   Installed Software:
#   - Microsoft Visual Studio Code
#       - C/C++ extension
#       - Serial Monitor extension
#       - PlatformIO IDE Extension
#
#   modified 2024-04-22
#   by Madison Gleydura
#
#   THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
#   FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#   COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
#   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#####################################################################################

$scriptPath = split-path -parent $MyInvocation.MyCommand.Definition

Write-Host "Destination SPACE Software Installer"

$exeArgs = '/verysilent /tasks=addcontextmenufiles,addcontextmenufolders,addtopath'

#Microsoft Visual Studio Code Installation
$vscodeFileUri = "https://code.visualstudio.com/sha/download?build=stable&os=win32-x64"
$vsCodeDestination = $scriptPath + "\VSCodeSetup-x64.exe"
$vsCodeExec = ($Env:PROGRAMFILES) + "\Microsoft VS Code\bin\code.cmd"
$extensions =@(
    "ms-vscode.cpptools",               #C/C++ Language Support
    "ms-vscode.vscode-serial-monitor",  #Serial Monitor Extension
    "platformio.platformio-ide"         #PlatformIO Support
) | SORT-OBJECT

Write-Host "Downloading Microsoft Visual Studio Code..."
$vsCodeBitsJobObj = Start-BitsTransfer $vscodeFileUri -Destination $vsCodeDestination

switch($vsCodeBitsJobObj.JobState){
    'Transferred'{
        Complete-BitsTransfer -BitsJob $vsCodeBitsJobObj
        break
    }
    'Error'{
        throw 'Error downloading Microsoft Visual Studio Code'
    }
}

Write-Host "Installing Microsoft Visual Studio Code..."
Start-Process -Wait $vsCodeDestination -ArgumentList $exeArgs

$extensions | ForEach-Object{
    try{
        Invoke-Expression "& '$vsCodeExec' --install-extension $_ --force"
        Write-Host
    }
    catch{
        $_
        Exit(1)
    }
}

Exit(0)
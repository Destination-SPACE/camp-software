# Camp-Software

This repository contains PowerShell scripts to install the required programs for Destination SPACE Satellite Week camps.

## Installation
1. Download your .ps1 file
2. Right-click the .ps1 file and click `Run With PowerShell`
3. Follow the installation instructions where applicable

## Camp Installers
- [Challenger Learning Center - Kentucky](/2024-CLCKY/Destination-SPACE-Camp-Software-Installer.ps1)
- [Fairmont State University](/2024-FSU/Destination-SPACE-Camp-Software-Installer.ps1)
- [Governor's School for Science and Mathematics](/2024-GSSM/Destination-SPACE-Camp-Software-Installer.ps1)
- [University of North Carolina - Asheville](/2024-UNCA/Destination-SPACE-Camp-Software-Installer.ps1)
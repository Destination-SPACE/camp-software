#include <Arduino.h>
#include <Adafruit_PM25AQI.h>

Adafruit_PM25AQI aqi = Adafruit_PM25AQI();

void setup() {
  Serial.begin(115200);
  while(!Serial);

  Serial.print("\nAdafruit PMSA003I Air Quality Sensor");
  
  delay(1000);

  if(!aqi.begin_I2C()){
    Serial.print("\nCould not find PM 2.5 sensor!");
    while(true){
      delay(10);
    }
  }

  Serial.print("\nPM25 found!");
}

void loop() {
  PM25_AQI_Data data;
  
  if(!aqi.read(&data)){
    Serial.print("\nCould not read from AQI");
    delay(500);
    return;
  }

  Serial.print("AQI reading success");
  Serial.print(F("\n\n---------------------------------------"));
  Serial.print(F("\nConcentration Units (standard)"));
  Serial.print(F("\n---------------------------------------"));
  Serial.print(F("\nPM 1.0: ")); Serial.print(data.pm10_standard);
  Serial.print(F("\t\tPM 2.5: ")); Serial.print(data.pm25_standard);
  Serial.print(F("\t\tPM 10: ")); Serial.print(data.pm100_standard);
  Serial.print(F("\nConcentration Units (environmental)"));
  Serial.print(F("\n---------------------------------------"));
  Serial.print(F("\nPM 1.0: ")); Serial.print(data.pm10_env);
  Serial.print(F("\t\tPM 2.5: ")); Serial.print(data.pm25_env);
  Serial.print(F("\t\tPM 10: ")); Serial.print(data.pm100_env);
  Serial.print(F("\n---------------------------------------"));
  Serial.print(F("\nParticles > 0.3um / 0.1L air:")); Serial.print(data.particles_03um);
  Serial.print(F("\nParticles > 0.5um / 0.1L air:")); Serial.print(data.particles_05um);
  Serial.print(F("\nParticles > 1.0um / 0.1L air:")); Serial.print(data.particles_10um);
  Serial.print(F("\nParticles > 2.5um / 0.1L air:")); Serial.print(data.particles_25um);
  Serial.print(F("\nParticles > 5.0um / 0.1L air:")); Serial.print(data.particles_50um);
  Serial.print(F("\nParticles > 10 um / 0.1L air:")); Serial.print(data.particles_100um);
  Serial.print(F("\n---------------------------------------"));

  delay(1000);
}
#include <Arduino.h>
#include <SHTSensor.h>
#include <Wire.h>

SHTSensor sht(SHTSensor::SHT85);

void setup() {
  Serial.begin(115200);
  Wire.begin();
  while(!Serial);
  
  delay(1000);

  if(sht.init()){
    Serial.print("\ninit(): success");
  }
  else{
    Serial.print("\ninit(): failed");
  }

  sht.setAccuracy(SHTSensor::SHT_ACCURACY_HIGH);
}

void loop() {
  if(sht.readSample()){
    Serial.print("\n\nSHT:");
    Serial.print("\n  RH: ");
    Serial.print(sht.getHumidity(), 2);
    Serial.print("\n  T:  ");
    Serial.print(sht.getTemperature(), 2);
  }
  else{
    Serial.print("\nError in readSample()");
  }

  delay(1000);
}
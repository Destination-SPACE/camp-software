#include <Arduino.h>
#include <Adafruit_SCD30.h>

Adafruit_SCD30 scd30;

void setup() {
  Serial.begin(115200);
  while(!Serial);

  Serial.print("\nAdafruit SCD30 test!");

  if(!scd30.begin()){
    Serial.print("\nFailed to find SCD30 sensor");
    while(true){
      delay(10);
    }
  }

  Serial.print("\nSCD30 found!");
  Serial.print("\nMeasurement Interval: ");
  Serial.print(scd30.getMeasurementInterval());
  Serial.print("seconds");
}

void loop() {
  if(scd30.dataReady()){
    Serial.print("\nData available!");

    if(!scd30.read()){
      Serial.print("\nError reading sensor data");
      return;
    }

    Serial.print("\nTemperature: ");
    Serial.print(scd30.temperature);
    Serial.print(" degrees C");

    Serial.print("\nRelative Humidity: ");
    Serial.print(scd30.relative_humidity);
    Serial.print(" %");

    Serial.print("\nCO2: ");
    Serial.print(scd30.CO2, 3);
    Serial.print(" ppm\n");
  }
  
  delay(100);
}